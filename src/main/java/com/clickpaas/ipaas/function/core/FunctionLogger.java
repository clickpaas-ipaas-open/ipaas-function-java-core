package com.clickpaas.ipaas.function.core;

/**
 * <p>
 * Copyright: Copyright (c) 2021/11/10 5:42 下午
 * <p>
 * Company: 苏州渠成易销网络科技有限公司
 * <p>
 *
 * @version 1.0.0
 * @className: FunctionLogger
 * @description: TODO 类描述
 * @author: heng.zhang@successchannel.com
 */
public interface FunctionLogger {

    /**
     * ipaas function log interface
     *
     * @param var1
     */
    void log(String var1);
}
