package com.clickpaas.ipaas.function.core;


import java.util.Map;

/**
 * <p>
 * Copyright: Copyright (c) 2021/11/10 5:28 下午
 * <p>
 * Company: 苏州渠成易销网络科技有限公司
 * <p>
 *
 * @version 1.0.0
 * @className: FaasHandler
 * @description: TODO 类描述
 * @author: heng.zhang@successchannel.com
 */
public interface FunctionHandler<I, O> {

    /**
     * <p>function handler</>
     *
     * @param input
     * @param context
     * @return 
     */
    O handle(I input, Map<String, String> context);

}
